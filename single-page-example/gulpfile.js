const gulp = require("gulp");
const sass = require("gulp-sass");
const sassGlob = require("gulp-sass-glob");
const del = require("del");

gulp.task("styles", () => {
  return gulp
    .src("sass/*.scss")
    .pipe(sassGlob())
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./public/css/"));
});

gulp.task("clean", () => {
  return del(["public/css/main.css"]);
});

gulp.task("default", gulp.series(["clean", "styles"]));
