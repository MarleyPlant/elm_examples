# ELM 0.18.0 Websocket Chat


## Installation
To install the required packages make sure you have ELM `0.18.0` and NPM install and then run:
```
npm install
```


## Building
To build the app for the server you need to run:
```
npm run build
```


## Starting Server
To Start the server run:
```
npm start
```