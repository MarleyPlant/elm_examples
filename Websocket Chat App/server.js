const express = require('express');
const app = express();
const ws = require('express-ws')(app);
const path = require('path');
const PORT = 8080;
const CLIENTS = [];

app.listen(PORT, (() => {
	console.log(`Listening on port ${PORT}`);
}));

app.ws('/connect', (websocket, request) => {
	console.log('Client connected');
	CLIENTS.push(websocket);
	websocket.on('message', (message) => {
		for(const prop of CLIENTS) {
			if (prop.readyState === 1) {
                now = new Date();
                time = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
				prop.send(`${time} ${message}`);
			}
		}
	});
});

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname+'/index.html'));
});